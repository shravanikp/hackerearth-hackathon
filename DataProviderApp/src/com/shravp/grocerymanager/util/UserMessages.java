package com.shravp.grocerymanager.util;

public enum UserMessages {
	cannotDeleteCategory("Category has some Items associated to it. Cannot be deleted until all associations have been removed."),
	cannotAddDuplicateService("Service with the same name exists. Cannot add duplicates"),
	cannotAddDuplicateCategory("Category with the same name exists. Cannot add duplicates"),
	cannotAddDuplicateItem("Item with the same name exists. Cannot add duplicates"),
	cannotDeleteItem("Items are still present in your Inventory. Cannot delete until all the stock is completed."),
	needToChooseCategory("Choose a valid Category"),
	needToChooseService("Choose a valid Service"),
	needToChooseUnits("Choose a valid Unit"),
	needToChooseNeedsRefill("Choose if you need a refill now"),
	needToProvideName("Provide name"),
	needToProvideDetails("Provide Details"),
	needToProvideQuantity("Provide Quantity"),
	needToProvideRefillAfter("Provide Refill After"),
	selectAtleastOneItem("Select atleast 1 Item"),
	needToChooseThresholdValue("Provide the Refill Threshold value"),
	yes("Yes"),
	no("No") ;

	private String displayMessage;

	private UserMessages(String displayMessage) {
		this.displayMessage = displayMessage;
	}

	public String getDisplayMessage() {
		return displayMessage;
	}
}
