package com.shravp.grocerymanager.util;

public enum ParseActions {
	add("ADD"),
	update("UPDATE"),
	delete("DELETE");

	private String actionName;

	private ParseActions(String actionName) {
		this.actionName = actionName;
	}

	public String getActionName() {
		return actionName;
	}
}
