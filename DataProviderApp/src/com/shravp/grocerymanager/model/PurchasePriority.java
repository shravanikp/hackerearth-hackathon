package com.shravp.grocerymanager.model;

import java.io.Serializable;

public class PurchasePriority implements Serializable {
	private String id;
	private String name;
	private String createdAt;
	private String updatedAt;
	public String getId() {
		return id;
	}
	
	public PurchasePriority(String id, String name) {		
		this.id = id;
		this.name = name;
	}
 
	public PurchasePriority(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

	
	@Override
	public String toString() {
		return "PurchasePriority [id=" + id + ", name=" + name + ", createdAt="
				+ createdAt + ", updatedAt=" + updatedAt + "]";
	}
	
	

}
