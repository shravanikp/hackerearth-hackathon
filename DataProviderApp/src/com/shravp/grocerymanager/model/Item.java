package com.shravp.grocerymanager.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Item  implements Parcelable {
	private String id;
	private String name;	
	private ItemType itemType;
	private String details;
	private String units;
	private Integer quantity;
	private Integer refillAfter;	
	private Boolean needsRefill;
	private Integer refillThreshold;
	private String createdAt;
	private String updatedAt;

	public Item(String name, ItemType itemType, String details, String units,
			Integer quantity, Integer refillAfter, Boolean needsRefill,Integer refillThreshold) {
		this.name = name;
		this.itemType = itemType;
		this.details = details;
		this.units = units;
		this.quantity = quantity;
		this.refillAfter = refillAfter;
		this.needsRefill = needsRefill;		
		this.refillThreshold = refillThreshold;
	}

	public Item(String id, String name, ItemType itemType, String details,
			String units, Integer quantity, Integer refillAfter,
			Boolean needsRefill,Integer refillThreshold,String createdAt,String updatedAt) {		
		this.id = id;
		this.name = name;
		this.itemType = itemType;
		this.details = details;
		this.units = units;
		this.quantity = quantity;
		this.refillAfter = refillAfter;
		this.needsRefill = needsRefill;
		this.refillThreshold = refillThreshold;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public Item(Parcel inSource) {
		id = inSource.readString();
		name = inSource.readString();
		itemType = inSource.readParcelable(ItemType.class.getClassLoader());
		details = inSource.readString();
		units = inSource.readString();
		quantity = inSource.readInt();
		refillAfter = inSource.readInt();
		needsRefill = (Boolean) inSource.readValue(Boolean.class.getClassLoader());
		refillThreshold = (Integer) inSource.readInt();
		createdAt = inSource.readString();
		updatedAt = inSource.readString();	
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public ItemType getItemType() {
		return itemType;
	}

	public String getDetails() {
		return details;
	}

	public String getUnits() {
		return units;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public Integer getRefillAfter() {
		return refillAfter;
	}

	public Boolean getNeedsRefill() {
		return needsRefill;
	}

	public Integer getThresholdFrequency() {
		return refillThreshold;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setRefillAfter(Integer refillAfter) {
		this.refillAfter = refillAfter;
	}

	public void setNeedsRefill(Boolean needsRefill) {
		this.needsRefill = needsRefill;
	}
	
	public Integer getRefillThreshold() {
		return refillThreshold;
	}

	public void setRefillThreshold(Integer refillThreshold) {
		this.refillThreshold = refillThreshold;
	}
	
	@Override
	public String toString() {
		return "Item [id=" + id + ", name=" + name + ", itemType=" + itemType
				+ ", details=" + details + ", units=" + units + ", quantity="
				+ quantity + ", refillAfter=" + refillAfter + ", needsRefill="
				+ needsRefill + ", refillThreshold=" + refillThreshold
				+ ", createdAt=" + createdAt + ", updatedAt=" + updatedAt + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((details == null) ? 0 : details.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((itemType == null) ? 0 : itemType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((needsRefill == null) ? 0 : needsRefill.hashCode());
		result = prime * result
				+ ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result
				+ ((refillAfter == null) ? 0 : refillAfter.hashCode());
		result = prime * result
				+ ((refillThreshold == null) ? 0 : refillThreshold.hashCode());
		result = prime * result + ((units == null) ? 0 : units.hashCode());
		result = prime * result
				+ ((updatedAt == null) ? 0 : updatedAt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (details == null) {
			if (other.details != null)
				return false;
		} else if (!details.equals(other.details))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itemType == null) {
			if (other.itemType != null)
				return false;
		} else if (!itemType.equals(other.itemType))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (needsRefill == null) {
			if (other.needsRefill != null)
				return false;
		} else if (!needsRefill.equals(other.needsRefill))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (refillAfter == null) {
			if (other.refillAfter != null)
				return false;
		} else if (!refillAfter.equals(other.refillAfter))
			return false;
		if (refillThreshold == null) {
			if (other.refillThreshold != null)
				return false;
		} else if (!refillThreshold.equals(other.refillThreshold))
			return false;
		if (units == null) {
			if (other.units != null)
				return false;
		} else if (!units.equals(other.units))
			return false;
		if (updatedAt == null) {
			if (other.updatedAt != null)
				return false;
		} else if (!updatedAt.equals(other.updatedAt))
			return false;
		return true;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(Parcel outSource, int flags) {
		outSource.writeString(id);
		outSource.writeString(name);
		outSource.writeParcelable(itemType, flags);
		outSource.writeString(details);
		outSource.writeString(units);
		outSource.writeInt(quantity);
		outSource.writeInt(refillAfter);
		outSource.writeValue(needsRefill);		
		outSource.writeInt(refillThreshold);
		outSource.writeString(createdAt);
		outSource.writeString(updatedAt);

	}
	
	public static final Parcelable.Creator<Item> CREATOR
	= new Parcelable.Creator<Item>() {
		public Item createFromParcel(Parcel in) {
			return new Item(in);
		}

		public Item[] newArray(int size) {
			return new Item[size];
		}
	};
}
