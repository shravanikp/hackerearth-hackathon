package com.shravp.grocerymanager.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemType implements Parcelable{
	private String id;
	private String name;
	private ServiceType serviceType;
	private String createdAt;
	private String updatedAt;

	public ItemType(String id,String name,ServiceType serviceType,String createdAt,String updatedAt) {	
		this.id =id;
		this.name = name;		
		this.serviceType = serviceType;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public ItemType(String name, ServiceType serviceType) {
		this.name = name;
		this.serviceType = serviceType;
	}

	private ItemType(Parcel inSource) {
		id = inSource.readString();
		name = inSource.readString();
		serviceType = inSource.readParcelable(ServiceType.class.getClassLoader());
		createdAt = inSource.readString();
		updatedAt = inSource.readString();
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	@Override
	public String toString() {
		return "ItemType [id=" + id + ", name=" + name + ", serviceType="
				+ serviceType + ", createdAt=" + createdAt + ", updatedAt="
				+ updatedAt + "]";
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((serviceType == null) ? 0 : serviceType.hashCode());
		result = prime * result
				+ ((updatedAt == null) ? 0 : updatedAt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemType other = (ItemType) obj;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (serviceType == null) {
			if (other.serviceType != null)
				return false;
		} else if (!serviceType.equals(other.serviceType))
			return false;
		if (updatedAt == null) {
			if (other.updatedAt != null)
				return false;
		} else if (!updatedAt.equals(other.updatedAt))
			return false;
		return true;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(Parcel outSource, int flags) {
		outSource.writeString(id);
		outSource.writeString(name);
		outSource.writeParcelable(serviceType, flags);
		outSource.writeString(createdAt);
		outSource.writeString(updatedAt);

	}
	
	public static final Parcelable.Creator<ItemType> CREATOR
	= new Parcelable.Creator<ItemType>() {
		public ItemType createFromParcel(Parcel in) {
			return new ItemType(in);
		}

		public ItemType[] newArray(int size) {
			return new ItemType[size];
		}
	};
}
