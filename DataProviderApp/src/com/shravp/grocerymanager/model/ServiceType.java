package com.shravp.grocerymanager.model;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceType implements Parcelable{
	private String id;
	private String name;
	private String details;
	private String createdAt;
	private String updatedAt;


	public ServiceType(String name, String details) {		
		this.name = name;
		this.details = details;		
	}


	public ServiceType(String id, String name, String details,String createdAt,String updatedAt) {		
		this.id = id;
		this.name = name;
		this.details = details;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}
	
	private ServiceType(Parcel inSource) {
		id = inSource.readString();
		name = inSource.readString();
		details = inSource.readString();
		createdAt = inSource.readString();
		updatedAt = inSource.readString();
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDetails() {
		return details;
	}


	public void setDetails(String details) {
		this.details = details;
	}


	public String getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


	public String getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}


	@Override
	public String toString() {
		return "ServiceType [id=" + id + ", name=" + name + ", details="
				+ details + ", createdAt=" + createdAt + ", updatedAt="
				+ updatedAt + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((details == null) ? 0 : details.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((updatedAt == null) ? 0 : updatedAt.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceType other = (ServiceType) obj;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (details == null) {
			if (other.details != null)
				return false;
		} else if (!details.equals(other.details))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (updatedAt == null) {
			if (other.updatedAt != null)
				return false;
		} else if (!updatedAt.equals(other.updatedAt))
			return false;
		return true;
	}


	@Override
	public int describeContents() {
		return this.hashCode();
	}


	@Override
	public void writeToParcel(Parcel outSource, int flags) {
		outSource.writeString(id);
		outSource.writeString(name);
		outSource.writeString(details);
		outSource.writeString(createdAt);
		outSource.writeString(updatedAt);

	}
	
	public static final Parcelable.Creator<ServiceType> CREATOR
	= new Parcelable.Creator<ServiceType>() {
		public ServiceType createFromParcel(Parcel in) {
			return new ServiceType(in);
		}

		public ServiceType[] newArray(int size) {
			return new ServiceType[size];
		}
	};

	
}
