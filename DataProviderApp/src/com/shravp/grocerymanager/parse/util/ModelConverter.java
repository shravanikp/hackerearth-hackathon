package com.shravp.grocerymanager.parse.util;

import com.parse.ParseObject;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.model.ItemType;
import com.shravp.grocerymanager.model.PurchasePriority;
import com.shravp.grocerymanager.model.ServiceType;
import com.shravp.grocerymanager.util.ParseActions;


public class ModelConverter {

	public ItemType getItemType(ParseObject itemTypeObject){
		return new ItemType(itemTypeObject.getObjectId(),itemTypeObject.getString("name")
				,getServiceType(itemTypeObject.getParseObject("serviceType")),
				itemTypeObject.getCreatedAt().toGMTString(),itemTypeObject.getUpdatedAt().toGMTString());
	}

	public Item getItem(ParseObject itemObject) {		
		return new Item(itemObject.getObjectId(), itemObject.getString("name"),
				getItemType(itemObject.getParseObject("itemType")), itemObject.getString("details"), 
				itemObject.getString("units"),itemObject.getNumber("quantity").intValue(),
				itemObject.getNumber("refillAfter").intValue(),itemObject.getBoolean("needsRefill"), itemObject.getNumber("refillThreshold").intValue(),
				itemObject.getCreatedAt().toGMTString(),itemObject.getUpdatedAt().toGMTString());		
	}

	public  ServiceType getServiceType(ParseObject serviceObject){			 
		return new ServiceType(serviceObject.getObjectId(),serviceObject.getString("name"), serviceObject.getString("details"),
				serviceObject.getCreatedAt().toGMTString(),serviceObject.getUpdatedAt().toGMTString());
	}

	public PurchasePriority getPurchasePriority(ParseObject purchaseObject){
		return new PurchasePriority(purchaseObject.getObjectId(), purchaseObject.getString("name"));

	}

	public ParseObject convertItemType(ItemType itemType, String action){
		ParseObject object = new ParseObject("ItemType");
		if((action.equals(ParseActions.update.getActionName())) || (action.equals(ParseActions.delete.getActionName()))) {
			object.setObjectId( itemType.getId());			
			object.put("serviceType", convertServiceType(itemType.getServiceType(),action));
		}
		else{
			/**
			 * If its an action to add a new Item Type, do not recreate the existing service type object
			 */					
			object.put("serviceType", convertServiceType(itemType.getServiceType(),ParseActions.update.getActionName()));
		}
		object.put("name", itemType.getName().toLowerCase());	

		return object;
	}

	public ParseObject convertItem(Item item, String action){
		ParseObject object = new ParseObject("Item");
		if((action.equals(ParseActions.update.getActionName())) || (action.equals(ParseActions.delete.getActionName()))) {
			object.setObjectId(item.getId());	
			object.put("itemType", convertItemType(item.getItemType(),action));
		}
		else{
			/**
			 * If its an action to add a new Item , do not recreate the existing Item Type object
			 */
			object.put("itemType", convertItemType(item.getItemType(),ParseActions.update.getActionName()));
		}
		object.put("name", item.getName().toLowerCase());				
		object.put("details", item.getDetails().toLowerCase());
		object.put("units", item.getUnits().toLowerCase());
		object.put("quantity", item.getQuantity());				
		object.put("refillAfter", item.getRefillAfter());
		object.put("needsRefill", item.getNeedsRefill());
		object.put("refillThreshold", item.getRefillThreshold());
		return object;
	}

	public ParseObject convertServiceType(ServiceType serviceType, String action) {
		ParseObject object = new ParseObject("ServiceType");
		if((action.equals(ParseActions.update.getActionName())) || (action.equals(ParseActions.delete.getActionName()))) {
			object.setObjectId(serviceType.getId());	
		}
		object.put("name", serviceType.getName().toLowerCase());	
		object.put("details", serviceType.getDetails().toLowerCase());
		return object;

	}

	public ParseObject convertPurchasePriority(PurchasePriority priority, String action){
		ParseObject object = new ParseObject("PurchasePriority");
		if((action.equals(ParseActions.update.getActionName())) || (action.equals(ParseActions.delete.getActionName()))) {
			object.put("objectId", priority.getId());
		}
		object.put("name", priority.getName().toLowerCase());	
		return object;
	}

}
