package com.shravp.grocerymanager.parse.util;


import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.util.Log;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.model.ItemType;
import com.shravp.grocerymanager.model.PurchasePriority;
import com.shravp.grocerymanager.model.ServiceType;
import com.shravp.grocerymanager.util.ParseActions;
import com.shravp.grocerymanager.util.UserMessages;

public class ParseDataProvider {

	private ModelConverter converter;
	public ParseDataProvider(Context context) {
		converter = new ModelConverter();
	}


	public List<ItemType> getAllItemTypes() throws ParseException{
		ParseQuery<ParseObject> itemTypeQuery = ParseQuery.getQuery("ItemType");
		//	itemTypeQuery.fromLocalDatastore();
		itemTypeQuery.include("serviceType");
		List<ParseObject> itemsTypesFromParse = itemTypeQuery.find();
		//	ParseObject.pinAll(itemsTypesFromParse);
		List<ItemType> itemTypes = new ArrayList<ItemType>();
		for(ParseObject itemType: itemsTypesFromParse) {
			itemTypes.add(converter.getItemType(itemType));
		}
		return itemTypes;
	}

	public List<Item> getAllItems() throws ParseException {
		ParseQuery<ParseObject> itemQuery = ParseQuery.getQuery("Item");
		/*itemQuery.fromLocalDatastore();*/
		itemQuery.include("itemType.serviceType");
		List<ParseObject> itemsFromParse = itemQuery.find();
		//	ParseObject.pinAll(itemsFromParse);
		List<Item> items = new ArrayList<Item>();
		for(ParseObject item : itemsFromParse) {
			items.add(converter.getItem(item));
		}
		return items;
	}

	public List<Item> getItemsByItemType(String itemTypeName) throws ParseException {
		List<Item> itemsToReturn  = new ArrayList<Item>();
		ItemType itemType = getItemTypeByName(itemTypeName);
		ParseQuery<ParseObject> itemQuery = ParseQuery.getQuery("Item");
		//itemQuery.fromLocalDatastore();
		itemQuery.include("itemType.serviceType");
		itemQuery.whereEqualTo("itemType", converter.convertItemType(itemType,ParseActions.delete.getActionName() ));
		List<ParseObject> items =  (List<ParseObject>) itemQuery.find();
		//ParseObject.pinAll(items);
		for(ParseObject item: items) {
			itemsToReturn.add(converter.getItem(item));
		}
		return itemsToReturn;
	}


	public List<String> getAllServiceTypes() throws ParseException {
		ParseQuery<ParseObject> serviceTypeQuery = ParseQuery.getQuery("ServiceType");
		//serviceTypeQuery.fromLocalDatastore();
		List<ParseObject> serviceTypesFromParse = serviceTypeQuery.find();
		//	ParseObject.pinAll(serviceTypesFromParse);
		List<String> serviceTypes = new ArrayList<String>();
		for(ParseObject serviceType : serviceTypesFromParse) {
			serviceTypes.add(converter.getServiceType(serviceType).getName());
		}
		return serviceTypes;
	}

	public ServiceType getServiceTypeByName(String serviceName) throws ParseException {		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("ServiceType");
		//	query.fromLocalDatastore();
		query.whereEqualTo("name", serviceName);
		List<ParseObject> serviceTypes =  (List<ParseObject>) query.find();
		//ParseObject.pinAll(serviceTypes);
		return (serviceTypes.size() > 0  ? converter.getServiceType(serviceTypes.get(0)) : null );
	}

	public ItemType getItemTypeByName(String itemTypeName) throws ParseException {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("ItemType");
		//	query.fromLocalDatastore();
		query.include("serviceType");
		query.whereEqualTo("name", itemTypeName);
		List<ParseObject> itemTypes =  (List<ParseObject>) query.find();
		//	ParseObject.pinAll(itemTypes);
		return (itemTypes.size() > 0 ? converter.getItemType(itemTypes.get(0)) : null);
	}

	public Item getItemByName(String itemName) throws ParseException {
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Item");
		//	query.fromLocalDatastore();
		query.include("itemType.serviceType");
		query.whereEqualTo("name", itemName);
		List<ParseObject> items =  (List<ParseObject>) query.find();
		//	ParseObject.pinAll(items);
		return (items.size() > 0 ? converter.getItem(items.get(0)) : null);
	}

	/*	public List<String> getAllPurchasePriority() throws ParseException {
		ParseQuery<ParseObject> purchasePriorityQuery = ParseQuery.getQuery("PurchasePriority");
		purchasePriorityQuery.fromLocalDatastore();
		List<ParseObject> itemsTypesFromParse = purchasePriorityQuery.find();
		List<String> purchasePriorities = new ArrayList<String>();
		for(ParseObject itemType: itemsTypesFromParse) {
			purchasePriorities.add(converter.getItemType(itemType).getName());
		}
		return purchasePriorities;
	}*/


	public boolean addServiceType(ServiceType serviceTypeToAdd) {
		boolean toReturn = false;
		try {
			ServiceType matchingService =  getServiceTypeByName(serviceTypeToAdd.getName().toLowerCase());
			if(matchingService == null || !(matchingService.getName().equalsIgnoreCase(serviceTypeToAdd.getName()))){
				converter.convertServiceType(serviceTypeToAdd, ParseActions.add.getActionName()).save();
				toReturn = true;
			}else {
				Log.d(getClass().getSimpleName(), UserMessages.cannotAddDuplicateService.getDisplayMessage());
				toReturn = false;
			}
		} catch (ParseException e) {			
			e.printStackTrace();
		}
		return toReturn;
	}


	public boolean addItemType(ItemType itemTypeToAdd) {
		boolean toReturn = false;
		try {
			ItemType matchingItemType = getItemTypeByName(itemTypeToAdd.getName().toLowerCase());			
			if(matchingItemType == null || !(matchingItemType.getName().equalsIgnoreCase(itemTypeToAdd.getName()))) {
				converter.convertItemType(itemTypeToAdd, ParseActions.add.getActionName()).save();
				toReturn = true;
			} else {
				Log.d(getClass().getSimpleName(), UserMessages.cannotAddDuplicateCategory.getDisplayMessage());
				toReturn  = false;
			}
		} catch (ParseException e) {			
			e.printStackTrace();
		}
		return toReturn;
	}


	public boolean addItem(Item itemToAdd) {
		boolean toReturn = false;
		try {		
			Item matchingItem = getItemByName(itemToAdd.getName().toLowerCase());			
			if(matchingItem == null || !(matchingItem.getName().equalsIgnoreCase(itemToAdd.getName()))){
				converter.convertItem(itemToAdd, ParseActions.add.getActionName()).save();
				toReturn = true;
			}else {
				Log.d(getClass().getSimpleName(), UserMessages.cannotAddDuplicateItem.getDisplayMessage());
				toReturn = false;
			}
		} catch (ParseException e) {			
			e.printStackTrace();
		}
		return toReturn;
	}



	public void addPurchasePriority(PurchasePriority purchasePriorityToAdd) {
		try {
			converter.convertPurchasePriority(purchasePriorityToAdd, ParseActions.add.getActionName()).save();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public List<Item> getItemsToShop() throws ParseException{
		ParseQuery<ParseObject> itemsToShopQuery = ParseQuery.getQuery("Item");
		/*itemsToShopQuery.fromLocalDatastore();*/
		itemsToShopQuery.include("itemType.serviceType");
		itemsToShopQuery.whereEqualTo("needsRefill", Boolean.TRUE);
		List<ParseObject> itemsFromParse = itemsToShopQuery.find();
		//ParseObject.pinAllInBackground(itemsFromParse);
		List<Item> items = new ArrayList<Item>();	 
		for(ParseObject item : itemsFromParse) {

			items.add(converter.getItem(item));
		}
		return items;	
	}


	/*
	 * Will be used to update items from the Bought Item Workflow or the Ignore Item Workflow
	 * to update the refillAfter and needsRefill fields
	 */
	public List<Item> saveItems(List<Item> itemsToSave) throws ParseException {
		List<Item> itemsAfterSave = new ArrayList<Item>();
		for(Item itemToSave: itemsToSave){
			converter.convertItem(itemToSave, ParseActions.update.getActionName()).save();
			itemsAfterSave.add(getItemByName(itemToSave.getName()));
		}
		return itemsAfterSave;
	}

	public void saveItem(Item itemToSave) throws ParseException {
		converter.convertItem(itemToSave, ParseActions.update.getActionName()).save();
	}

	public void saveItemType(ItemType itemTypeToSave) throws ParseException {
		converter.convertItemType(itemTypeToSave, ParseActions.update.getActionName()).save();
	}

	public void deleteItemType(ItemType itemTypeToDelete) throws ParseException{
		converter.convertItemType(itemTypeToDelete, ParseActions.delete.getActionName()).delete();
	}

	public void deleteItemType(ParseObject objectToDelete) throws ParseException{
		objectToDelete.delete();
	}

	public void deleteServiceType(ServiceType serviceTypeToDelete) throws ParseException{
		converter.convertServiceType(serviceTypeToDelete, ParseActions.delete.getActionName()).delete();
	}

	public void deleteServiceType(ParseObject objectToDelete) throws ParseException{
		objectToDelete.delete();
	}

	public void deleteItem(Item itemToDelete) throws ParseException{
		converter.convertItem(itemToDelete, ParseActions.delete.getActionName()).delete();
	}

	public void deleteItem(ParseObject objectToDelete) throws ParseException{
		objectToDelete.delete();
	}

	public ModelConverter getConverter() {
		return converter;
	}
}
