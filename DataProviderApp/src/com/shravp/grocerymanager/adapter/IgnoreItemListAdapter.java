package com.shravp.grocerymanager.adapter;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.model.Item;

public class IgnoreItemListAdapter extends ArrayAdapter<Item>  {
	
	private Context context;
	private List<Item> ignoreItems;
	int layoutResID;
	private List<String> unitTypes = new ArrayList<String>();
	
	public IgnoreItemListAdapter(Context context, int layoutResID, List<Item> boughtItems) {
		super(context, layoutResID,  boughtItems);
		this.context = context;
		this.ignoreItems = boughtItems;
		this.layoutResID = layoutResID;
		
	}
	@Override
	  public View getView ( int position, View convertView, ViewGroup parent ) {
		LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = ( RelativeLayout ) infalInflater.inflate(R.layout.ignore_items_details,null);
		
	    Item item = ignoreItems.get(position);
	   
	    TextView itemName = (TextView) convertView.findViewById(R.id.ignoreItemName);
	    itemName.setText(item.getName());
	    
	    TextView itemUnits = (TextView)convertView.findViewById(R.id.ignoreItemUnits);
	    itemUnits.setText(item.getUnits());
	    	
	    TextView itemQuantity = (TextView)convertView.findViewById(R.id.ignoreItemQuantity);        
        itemQuantity.setText(item.getQuantity().toString());

        EditText itemRefillAfter = (EditText)convertView.findViewById(R.id.ignoreItemRefillAfter);       
        itemRefillAfter.setText(item.getRefillAfter().toString());

	    return convertView;
	  }
	
	
	@Override
    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }


}
