package com.shravp.grocerymanager.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.model.ItemType;

public class ManageItemsExpandableAdapter  extends BaseExpandableListAdapter{
	private Context context;
	private List<ItemType> dataHeader = new ArrayList<ItemType>();
	private HashMap<ItemType,List<Item>> dataChildren = new HashMap<ItemType,List<Item>>();


	public ManageItemsExpandableAdapter(Context context, List<ItemType> dataHeader,
			HashMap<ItemType,List<Item>> dataChildren) {
		this.context = context;
		this.dataHeader = dataHeader;
		this.dataChildren = dataChildren;
	}

	@Override
	public int getGroupCount() {		
		return dataHeader.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {		
		return dataChildren.get(dataHeader.get(groupPosition)).size();
	}

	@Override
	public ItemType getGroup(int groupPosition) {		
		return dataHeader.get(groupPosition);
	}

	@Override
	public Item getChild(int groupPosition, int childPosition) {
		return dataChildren.get(dataHeader.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {		
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,	View convertView, ViewGroup parent) { 
		ItemType item = (ItemType) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group_layout, null);
		}
		TextView listHeader = (TextView) convertView.findViewById(R.id.groupHeader);
		listHeader.setTypeface(null, Typeface.BOLD);
		listHeader.setText(item.getName());		
		
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,boolean isLastChild, View convertView, ViewGroup parent) { 
		final Item item = (Item) getChild(groupPosition, childPosition);			 
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_manage_item_layout, null);
		}	 
		TextView childList = (TextView) convertView.findViewById(R.id.listItems);	 
		childList.setText(item.getName());
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
	   	    return true;
	}
	
	
	
	@Override
    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }

}
