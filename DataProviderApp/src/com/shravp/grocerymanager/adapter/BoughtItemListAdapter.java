package com.shravp.grocerymanager.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.model.Item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class BoughtItemListAdapter extends ArrayAdapter<Item> {
	
	private Context context;
	private List<Item> boughtItems;
	int layoutResID;
	private List<String> unitTypes = new ArrayList<String>();
	
	public BoughtItemListAdapter(Context context, int layoutResID, List<Item> boughtItems) {
		super(context, layoutResID,  boughtItems);
		this.context = context;
		this.boughtItems = boughtItems;
		this.layoutResID = layoutResID;
		
	}
	@Override
	  public View getView ( int position, View convertView, ViewGroup parent ) {
		LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = ( RelativeLayout ) infalInflater.inflate(R.layout.bought_items_detail,null);
		loadItemUnits();
		
	    Item item = boughtItems.get(position);
	   
	    TextView itemName = (TextView) convertView.findViewById(R.id.boughtItemName);
	    itemName.setText(item.getName());
	    
	    Spinner itemUnitsSpinner = (Spinner)convertView.findViewById(R.id.boughtItemUnits);
	    
	    ArrayAdapter<String> itemUnitsDataAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, unitTypes);
		itemUnitsDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		itemUnitsSpinner.setAdapter(itemUnitsDataAdapter);
		itemUnitsSpinner.setSelection(unitTypes.indexOf(item.getUnits()));
		
        EditText itemQuantity = (EditText)convertView.findViewById(R.id.boughtItemQuantity);        
        itemQuantity.setText(item.getQuantity().toString());

        EditText itemRefillAfter = (EditText)convertView.findViewById(R.id.boughtItemRefillAfter);       
        itemRefillAfter.setText(item.getRefillAfter().toString());

	    return convertView;
	  }
	
	/***
	 * Function to load all static content of Unit Type
	 */
	private void loadItemUnits(){
		unitTypes.clear();
		unitTypes.add("   ");
		unitTypes.add("sachets");
		unitTypes.add("grams");
		unitTypes.add("kilograms");
		unitTypes.add("litre");
		unitTypes.add("dozen");
		unitTypes.add("bottle");
		unitTypes.add("packet");
		Collections.sort(unitTypes);
	}
	
	@Override
    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
    }


}
