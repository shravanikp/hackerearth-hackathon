package com.shravp.grocerymanager.service;

import java.util.List;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.parse.util.ParseDataProvider;
import com.shravp.grocerymanager.util.ParseActions;
import android.app.IntentService;
import android.content.Intent;

public class DataCleansingService extends IntentService {

	public DataCleansingService() {
		super("data-cleansing-service");		
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		ParseDataProvider dataProvider = new ParseDataProvider(this);		
		try {
			List<Item> obtainedItems = dataProvider.getAllItems();
			for(Item obtainedItem: obtainedItems) {
				Integer refillAfter =((obtainedItem.getRefillAfter() > 0) ? (obtainedItem.getRefillAfter() - 1) : 0 );
				obtainedItem.setRefillAfter(refillAfter);				
				if(refillAfter <= obtainedItem.getRefillThreshold()) {
					obtainedItem.setNeedsRefill(true);
				}else{
					obtainedItem.setNeedsRefill(false);
				}				
				dataProvider.saveItem(obtainedItem);				
			}
		} catch (ParseException e) {			
			e.printStackTrace();
		}
	}

}
