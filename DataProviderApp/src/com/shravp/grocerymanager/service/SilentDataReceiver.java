package com.shravp.grocerymanager.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class SilentDataReceiver extends BroadcastReceiver {
	public static final int REQUEST_CODE = 12345;
	
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Toast.makeText(context, "Data Cleansing", Toast.LENGTH_SHORT).show();
		Intent i = new Intent(context, DataCleansingService.class);		
		context.startService(i);
	}
}
