package com.shravp.grocerymanager.activity;

import java.util.ArrayList;
import java.util.List;
import com.parse.ParseException;
import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.model.ItemType;
import com.shravp.grocerymanager.model.ServiceType;
import com.shravp.grocerymanager.parse.util.ParseDataProvider;
import com.shravp.grocerymanager.util.UserMessages;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class AddEditCategoryActivity extends Activity implements OnItemSelectedListener{
	private ImageButton saveButton;
	private Spinner serviceTypeSpinner;
	private EditText categoryNameEditText;
	private List<String> serviceTypes = new ArrayList<String>();
	private boolean isActionAdd = true;
	private ItemType  addItemType,editItemType;
	private ParseDataProvider dataProvider;
	private ServiceType sType ;
	private TextView manageCategoryTitleText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_edit_category);

		dataProvider = new ParseDataProvider(this);

		saveButton = (ImageButton)findViewById(R.id.saveItemBtn);
		serviceTypeSpinner = (Spinner) findViewById(R.id.serviceType);
		serviceTypeSpinner.setOnItemSelectedListener(this);
		categoryNameEditText = (EditText)findViewById(R.id.categoryName);
		manageCategoryTitleText = (TextView)findViewById(R.id.manageCategoryTitle);
		Intent intent = this.getIntent();

		loadServiceTypes();

		if (intent.getExtras() != null) {			
			isActionAdd = false;
			manageCategoryTitleText.setText(R.string.title_activity_edit_category);
			editItemType = (ItemType) intent.getParcelableExtra("com.shravp.dataapp.model.ItemType");
			categoryNameEditText.setText(editItemType.getName());		
			serviceTypeSpinner.setSelection(serviceTypes.indexOf(editItemType.getServiceType().getName()));		
			setTitle(getString(R.string.app_name));
		}
		else{
			isActionAdd = true;	
			manageCategoryTitleText.setText(R.string.title_activity_add_category);
			setTitle(getString(R.string.app_name));
		}

		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				saveItemType();
			}
		});

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home: {navigateBackToParent();}		
		}		
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Function to load Service Types from Parse
	 */
	private void loadServiceTypes(){
		serviceTypes.add("  ");
		try {
			serviceTypes.addAll(dataProvider.getAllServiceTypes());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, serviceTypes);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		serviceTypeSpinner.setAdapter(dataAdapter);
	}

	private void saveItemType(){
		try {
			sType = dataProvider.getServiceTypeByName(serviceTypes.get(serviceTypeSpinner.getSelectedItemPosition()));


			if(categoryNameEditText.getText() == null ||categoryNameEditText.getText().toString().trim().equals("")){
				Toast.makeText(getApplicationContext(),UserMessages.needToProvideName.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return ;
			}	
			else if(sType == null){
				Toast.makeText(getApplicationContext(),UserMessages.needToChooseService.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return;
			} 

			if(isActionAdd){											
				addItemType = new ItemType(categoryNameEditText.getText().toString(), sType);
				boolean isSaved = dataProvider.addItemType(addItemType);
				if(!isSaved){
					Toast.makeText(getApplicationContext(),UserMessages.cannotAddDuplicateCategory.getDisplayMessage(),Toast.LENGTH_LONG).show();
				}
				navigateBackToParent();
			} else {
				editItemType.setName(categoryNameEditText.getText().toString());
				editItemType.setServiceType(sType);
				dataProvider.saveItemType(editItemType);
				navigateBackToParent();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
		switch(parent.getId()){		
		case R.id.serviceType : {
			try {
				sType = dataProvider.getServiceTypeByName(serviceTypes.get(serviceTypeSpinner.getSelectedItemPosition()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			break;

		}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {


	}

	private boolean navigateBackToParent(){
		Intent upIntent = NavUtils.getParentActivityIntent(this);
		if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
			TaskStackBuilder.create(this)
			.addNextIntentWithParentStack(upIntent)
			.startActivities();
		} else {
			NavUtils.navigateUpTo(this, upIntent);
		}
		return true;
	}
}
