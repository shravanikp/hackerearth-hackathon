package com.shravp.grocerymanager.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import com.parse.Parse;
import com.parse.ParseException;
import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.adapter.ManageItemsExpandableAdapter;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.model.ItemType;
import com.shravp.grocerymanager.parse.util.ParseDataProvider;
import com.shravp.grocerymanager.util.UserMessages;

import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;

public class ManageItemActivity extends ExpandableListActivity {
	private ManageItemsExpandableAdapter listAdapter;
	private ExpandableListView expListView;
	private List<ItemType> listDataHeader = new ArrayList<ItemType>();
	private HashMap<ItemType, List<Item>> listDataChild = new HashMap<ItemType,List<Item>>();
	private ParseDataProvider dataProvider;
	private Item itemObtained;
	private ItemType itemTypeObtained;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_item);	

		dataProvider = new ParseDataProvider(this);

		expListView = (ExpandableListView) findViewById(android.R.id.list);		
		listAdapter = new ManageItemsExpandableAdapter(this, listDataHeader, listDataChild);        		
		expListView.setAdapter(listAdapter);

		expListView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {

				return false;
			}			
		});


		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
			@Override
			public void onGroupExpand(int groupPosition) {

			}
		});


		expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {
			@Override
			public void onGroupCollapse(int groupPosition) {

			}
		});


		expListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {
				return false;
			}
		});	

		try {
			refreshData();
		} catch (ParseException e) {		
			e.printStackTrace();
		}

		registerForContextMenu(expListView);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
	}

	private void refreshData() throws ParseException {	
		listDataChild.clear();
		listDataHeader.clear();
		setProgressBarIndeterminateVisibility(true);

		
		List<Item> itemsPresent = dataProvider.getAllItems();
		for(Item present: itemsPresent){
			List<Item> itemsList = null;
			setProgressBarIndeterminateVisibility(false);
			if(!listDataHeader.contains(present.getItemType())){
				listDataHeader.add(present.getItemType());
				itemsList = new ArrayList<Item>();
				itemsList.add(present);
				listDataChild.put(present.getItemType(), itemsList);
			}else{
				listDataChild.get(present.getItemType()).add(present);				
			}
		}
			
		//Obtain all those Item Types that don't have an Item associated to them for display
		List<ItemType> itemsTypes = dataProvider.getAllItemTypes();
	    itemsTypes.removeAll(listDataHeader);
			
        for(ItemType typeToAdd: itemsTypes){
        	listDataHeader.add(typeToAdd);
        	listDataChild.put(typeToAdd, Collections.EMPTY_LIST);
        }
		listAdapter.notifyDataSetChanged();
	}


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;

		int type = ExpandableListView.getPackedPositionType(info.packedPosition);
		int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
		int childPosition = ExpandableListView.getPackedPositionChild(info.packedPosition);

		if (type == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
			menu.setHeaderTitle(listAdapter.getGroup(groupPosition).getName());
			menu.add(Menu.NONE, 0, 0, "Edit");  
			menu.add(Menu.NONE, 1, 1, "Delete");			
		} else if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
			menu.setHeaderTitle(listAdapter.getChild(groupPosition, childPosition).getName());
			menu.add(Menu.NONE, 0, 0, "Edit");  
			menu.add(Menu.NONE, 1, 1, "Delete");
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem menu) {
		super.onContextItemSelected(menu);
		ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) menu
				.getMenuInfo();

		int type = ExpandableListView.getPackedPositionType(info.packedPosition);
		int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
		int childPosition = ExpandableListView.getPackedPositionChild(info.packedPosition);

		if (type == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
			itemTypeObtained = listAdapter.getGroup(groupPosition);
			if(menu.getTitle()=="Edit"){			      				
				Intent intent = new Intent(this, AddEditCategoryActivity.class);
				intent.putExtra("com.shravp.dataapp.model.ItemType", itemTypeObtained);
				startActivity(intent);
			}  			
			else if(menu.getTitle()=="Delete"){
				try {
					List<Item> itemsAssociatedWithType = (List<Item>) dataProvider.getItemsByItemType(itemTypeObtained.getName());
					if(itemsAssociatedWithType.size()  > 0){
						Toast.makeText(getApplicationContext(),UserMessages.cannotDeleteCategory.getDisplayMessage(),Toast.LENGTH_LONG).show();
					}
					else{
						AlertDialog.Builder userAlertBuilder = new AlertDialog.Builder(this);	
						userAlertBuilder.setTitle("Are you sure you want to delete the Category ?");
						userAlertBuilder					
						.setCancelable(false)
						.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								try {
									dataProvider.deleteItemType(itemTypeObtained);
									refreshData();
								} catch (ParseException e) {								
									e.printStackTrace();
								}
							}
						});
						userAlertBuilder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});

						AlertDialog userAlertDialog = userAlertBuilder.create();
						userAlertDialog.show();					

					}

				} catch (ParseException e) {
					e.printStackTrace();
				}

			}

		} else if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {  

			itemObtained = listAdapter.getChild(groupPosition, childPosition);
			if(menu.getTitle()=="Edit"){	
				Intent intent = new Intent(this, AddEditItemActivity.class);
				intent.putExtra("com.shravp.dataapp.model.Item", itemObtained);
				startActivity(intent);
			}  			
			else if(menu.getTitle()=="Delete"){		
				if(itemObtained.getQuantity()  > 0){
					Toast.makeText(getApplicationContext(),UserMessages.cannotDeleteItem.getDisplayMessage(),Toast.LENGTH_LONG).show();
				}
				else{
					AlertDialog.Builder userAlertBuilder = new AlertDialog.Builder(this);	
					userAlertBuilder.setTitle("Are you sure you want to delete the Item ?");
					userAlertBuilder					
					.setCancelable(false)
					.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							try {
								dataProvider.deleteItem(itemObtained);
								refreshData();
							} catch (ParseException e) {								
								e.printStackTrace();
							}
						}
					});
					userAlertBuilder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});

					AlertDialog userAlertDialog = userAlertBuilder.create();
					userAlertDialog.show();					

				}
			}
		}

		return true;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home: {
			Intent upIntent = NavUtils.getParentActivityIntent(this);
			if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
				TaskStackBuilder.create(this)
				.addNextIntentWithParentStack(upIntent)
				.startActivities();
			} else {
				NavUtils.navigateUpTo(this, upIntent);
			}
			return true;
		}
		case R.id.action_refresh: {
			try {
				refreshData();
			} catch (ParseException e) {			
				e.printStackTrace();
			} 
		}
		case R.id.action_new_item: {		
			Intent intent = new Intent(this, AddEditItemActivity.class);
			startActivity(intent);
			break;
		}
		case R.id.action_new_category: {		
			Intent intent = new Intent(this, AddEditCategoryActivity.class);
			startActivity(intent);
			break;
		}
		}		
		return super.onOptionsItemSelected(item);
	}


}
