package com.shravp.grocerymanager.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.parse.ParseException;
import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.model.ItemType;
import com.shravp.grocerymanager.parse.util.ParseDataProvider;
import com.shravp.grocerymanager.util.UserMessages;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class AddEditItemActivity extends Activity implements OnItemSelectedListener{
	private ImageButton saveButton;
	private EditText itemNameEditText;
	private Spinner itemTypeSpinner;
	private EditText itemDetailsEditText;
	private Spinner itemUnitsSpinner;
	private EditText itemQuantityEditText;
	private EditText itemRefillAfterEditText;
	private Spinner itemNeedsRefillSpinner;
	private EditText itemRefillThreshold;
	private List<String> itemTypes = new ArrayList<String>();
	private List<String> unitTypes  = new ArrayList<String>();
	private List<String> needsRefill = new ArrayList<String>();
	private boolean isActionAdd = true;
	private Item  addItem,editItem;
	private ParseDataProvider dataProvider;
	private ItemType iType ;
	private String iUnits,iNeedsRefill;
	private TextView manageItemTitleText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_edit_item);

		dataProvider = new ParseDataProvider(this);

		saveButton = (ImageButton)findViewById(R.id.saveItemBtn);
		manageItemTitleText = (TextView)findViewById(R.id.manageItemViewTitle);
		itemNameEditText = (EditText)findViewById(R.id.itemName);
		itemDetailsEditText = (EditText)findViewById(R.id.itemDetails);
		itemQuantityEditText = (EditText)findViewById(R.id.itemQuantity);
		itemRefillAfterEditText = (EditText)findViewById(R.id.itemRefillAfter);		
		itemRefillThreshold = (EditText)findViewById(R.id.itemRefillThreshold);

		itemTypeSpinner = (Spinner) findViewById(R.id.itemType);
		itemTypeSpinner.setOnItemSelectedListener(this);
		itemUnitsSpinner = (Spinner)findViewById(R.id.itemUnits);
		itemUnitsSpinner.setOnItemSelectedListener(this);
		itemNeedsRefillSpinner = (Spinner)findViewById(R.id.itemNeedsRefill);
		itemNeedsRefillSpinner.setOnItemSelectedListener(this);

		Intent intent = this.getIntent();

		loadItemTypes();
		loadItemUnits();
		loadNeedsRefill();

		if (intent.getExtras() != null) {			
			isActionAdd = false;
			manageItemTitleText.setText(getString(R.string.title_activity_edit_item));
			editItem = (Item) intent.getParcelableExtra("com.shravp.dataapp.model.Item");
			Log.d(getClass().getSimpleName(), "Edit Item obtained is: " +editItem);
			itemNameEditText.setText(editItem.getName());	
			itemDetailsEditText.setText(editItem.getDetails());

			itemQuantityEditText.setText(editItem.getQuantity().toString());
			itemRefillAfterEditText.setText(editItem.getRefillAfter().toString());
			itemRefillThreshold.setText(editItem.getRefillThreshold().toString());

			itemTypeSpinner.setSelection(itemTypes.indexOf(editItem.getItemType().getName()));	
			itemUnitsSpinner.setSelection(unitTypes.indexOf(editItem.getUnits()));
			itemNeedsRefillSpinner.setSelection(needsRefill.indexOf(editItem.getNeedsRefill().equals(Boolean.TRUE) 
					? UserMessages.yes.getDisplayMessage() : UserMessages.no.getDisplayMessage()));
    		setTitle(getString(R.string.app_name));
		}
		else{
			isActionAdd = true;	
			manageItemTitleText.setText(getString(R.string.title_activity_add_item));
			itemNeedsRefillSpinner.setSelection(needsRefill.indexOf(UserMessages.no.getDisplayMessage()));
			setTitle(getString(R.string.app_name));
		}

		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				saveItem();
			}
		});

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home: {navigateBackToParent();}		
		}		
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Function to load Service Types from Parse
	 */
	private void loadItemTypes(){
		itemTypes.add("    ");
		try {
			for(ItemType iType:dataProvider.getAllItemTypes()){
				itemTypes.add(iType.getName());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Collections.sort(itemTypes);

		ArrayAdapter<String> itemTypeDataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, itemTypes);
		itemTypeDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		itemTypeSpinner.setAdapter(itemTypeDataAdapter);
	}

	/***
	 * Function to load all static content of Unit Type
	 */
	private void loadItemUnits(){
		unitTypes.clear();
		unitTypes.add("   ");
		unitTypes.add("sachets");
		unitTypes.add("grams");
		unitTypes.add("kilograms");
		unitTypes.add("litre");
		unitTypes.add("dozen");
		unitTypes.add("bottle");
		unitTypes.add("packet");
		Collections.sort(unitTypes);
		ArrayAdapter<String> itemUnitsDataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, unitTypes);
		itemUnitsDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		itemUnitsSpinner.setAdapter(itemUnitsDataAdapter);

	}

	private void  loadNeedsRefill() {
		needsRefill.add("   ");
		needsRefill.add(UserMessages.yes.getDisplayMessage());
		needsRefill.add(UserMessages.no.getDisplayMessage());
		Collections.sort(needsRefill);

		ArrayAdapter<String> itemNeedsRefillDataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, needsRefill);
		itemNeedsRefillDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		itemNeedsRefillSpinner.setAdapter(itemNeedsRefillDataAdapter);
	}

	private void saveItem(){
		try {
			iType = dataProvider.getItemTypeByName(itemTypes.get(itemTypeSpinner.getSelectedItemPosition()));	
			iUnits = itemUnitsSpinner.getSelectedItem().toString();
			iNeedsRefill = itemNeedsRefillSpinner.getSelectedItem().toString();

			if(itemNameEditText.getText() == null ||itemNameEditText.getText().toString().trim().equals("")){
				Toast.makeText(getApplicationContext(),UserMessages.needToProvideName.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return ;
			}
			else if(iType == null){
				Toast.makeText(getApplicationContext(),UserMessages.needToChooseCategory.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return;
			}
			else if(itemDetailsEditText.getText() == null ||itemDetailsEditText.getText().toString().trim().equals("")){
				Toast.makeText(getApplicationContext(),UserMessages.needToProvideDetails.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return ;
			}
			else if(iUnits.trim().equals("")){
				Toast.makeText(getApplicationContext(),UserMessages.needToChooseUnits.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return;
			}
			else if(itemQuantityEditText.getText() == null ||itemQuantityEditText.getText().toString().trim().equals("")){
				Toast.makeText(getApplicationContext(),UserMessages.needToProvideQuantity.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return ;
			}else if(itemRefillAfterEditText.getText() == null ||itemRefillAfterEditText.getText().toString().trim().equals("")){
				Toast.makeText(getApplicationContext(),UserMessages.needToProvideRefillAfter.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return ;
			}
			else if(iNeedsRefill.trim().equals("")){
				Toast.makeText(getApplicationContext(),UserMessages.needToChooseNeedsRefill.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return ;
			}else if (itemRefillThreshold.getText() == null || itemRefillThreshold.getText().toString().trim().equals("")){
				Toast.makeText(getApplicationContext(),UserMessages.needToChooseThresholdValue.getDisplayMessage(),Toast.LENGTH_LONG).show();
				return ;
			}

			if(isActionAdd){				
				addItem = new Item(itemNameEditText.getText().toString(), iType, itemDetailsEditText.getText().toString(),
						iUnits, Integer.parseInt(itemQuantityEditText.getText().toString()),
						Integer.parseInt(itemRefillAfterEditText.getText().toString()), Boolean.getBoolean(itemNeedsRefillSpinner.getSelectedItem().toString()),
						Integer.parseInt(itemRefillThreshold.getText().toString()));				
				boolean isSaved = dataProvider.addItem(addItem);
				if(!isSaved){
					Toast.makeText(getApplicationContext(),UserMessages.cannotAddDuplicateItem.getDisplayMessage(),Toast.LENGTH_LONG).show();
				}
				navigateBackToParent();
			} else {				
				editItem.setName(itemNameEditText.getText().toString());
				editItem.setItemType(iType);
				editItem.setDetails(itemDetailsEditText.getText().toString());
				editItem.setUnits(iUnits);
				editItem.setQuantity(Integer.parseInt(itemQuantityEditText.getText().toString()));
				editItem.setRefillAfter(Integer.parseInt(itemRefillAfterEditText.getText().toString()));
				editItem.setNeedsRefill(Boolean.getBoolean(itemNeedsRefillSpinner.getSelectedItem().toString()));
				editItem.setRefillThreshold(Integer.parseInt(itemRefillThreshold.getText().toString()));
				dataProvider.saveItem(editItem);
				navigateBackToParent();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,long id) {
		switch(parent.getId()){		
		case R.id.itemType : {
			try {
				iType = dataProvider.getItemTypeByName(itemTypes.get(itemTypeSpinner.getSelectedItemPosition()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			break;

		}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {


	}

	private boolean navigateBackToParent(){
		Log.d(getClass().getSimpleName(), "navigateBackToParent");

		Intent upIntent = NavUtils.getParentActivityIntent(this);
		if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
			TaskStackBuilder.create(this)
			.addNextIntentWithParentStack(upIntent)
			.startActivities();
		} else {
			NavUtils.navigateUpTo(this, upIntent);
		}
		return true;

	}	
}
