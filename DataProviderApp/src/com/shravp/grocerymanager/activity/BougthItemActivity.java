package com.shravp.grocerymanager.activity;

import java.util.ArrayList;
import com.parse.ParseException;
import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.adapter.BoughtItemListAdapter;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.parse.util.ParseDataProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

public class BougthItemActivity extends Activity {
	private ListView boughtItemListView;
	private ParseDataProvider dataProvider;
	private BoughtItemListAdapter boughtItemsAdapter;
	private ArrayList<Item> boughtItemsList = new ArrayList<Item>();
	private ImageButton updateButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_bougth_item);

		dataProvider = new ParseDataProvider(this);      

		Intent intent = this.getIntent();
		boughtItemsList =intent.getParcelableArrayListExtra("boughtItems");

		boughtItemListView = (ListView)findViewById(R.id.itemsBoughtlist);
		boughtItemsAdapter = new BoughtItemListAdapter(this,R.layout.bought_items_detail,boughtItemsList);
		boughtItemListView.setAdapter(boughtItemsAdapter);

		updateButton = (ImageButton)findViewById(R.id.updateButtonBtn);

		registerForContextMenu(boughtItemListView);


		boughtItemsAdapter.notifyDataSetChanged();

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		updateButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int itemCount = boughtItemListView.getCount();			
				for(int i=0 ; i<itemCount; i++){
					Item itemAfterBought = (Item) boughtItemListView.getItemAtPosition(i);

					View updatedItemView = boughtItemListView.getChildAt(i);
					Spinner unitsSpinner = (Spinner) updatedItemView.findViewById(R.id.boughtItemUnits);
					EditText quantityEditText = (EditText) updatedItemView.findViewById(R.id.boughtItemQuantity);
					EditText refillAfterEditText = (EditText) updatedItemView.findViewById(R.id.boughtItemRefillAfter);

					itemAfterBought.setUnits(unitsSpinner.getSelectedItem().toString());
					itemAfterBought.setQuantity(Integer.parseInt(quantityEditText.getText().toString()));
					itemAfterBought.setRefillAfter(Integer.parseInt(refillAfterEditText.getText().toString()));
					itemAfterBought.setNeedsRefill(false);
					try {
						dataProvider.saveItem(itemAfterBought);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				navigateBackToParent();
				boughtItemsList.clear();				
			}

		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home: {navigateBackToParent();}		
		}		
		return super.onOptionsItemSelected(item);
	}

	private boolean navigateBackToParent(){
		Intent upIntent = NavUtils.getParentActivityIntent(this);
		if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
			TaskStackBuilder.create(this)
			.addNextIntentWithParentStack(upIntent)
			.startActivities();
		} else {
			NavUtils.navigateUpTo(this, upIntent);
		}
		return true;
	}

}
