package com.shravp.grocerymanager.activity;

import java.util.ArrayList;

import com.parse.ParseException;
import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.R.layout;
import com.shravp.grocerymanager.adapter.BoughtItemListAdapter;
import com.shravp.grocerymanager.adapter.IgnoreItemListAdapter;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.parse.util.ParseDataProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

public class IgnoreItemActivity extends Activity {
	private ListView ignoreItemListView;
	private ParseDataProvider dataProvider;
	private IgnoreItemListAdapter ignoreItemsAdapter;
	private ArrayList<Item> ignoreItemsList = new ArrayList<Item>();
	private ImageButton updateButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_bougth_item);

		dataProvider = new ParseDataProvider(this);      

		Intent intent = this.getIntent();
		ignoreItemsList =intent.getParcelableArrayListExtra("ignoreItems");

		ignoreItemListView = (ListView)findViewById(R.id.itemsBoughtlist);
		ignoreItemsAdapter = new IgnoreItemListAdapter(this,R.layout.ignore_items_details,ignoreItemsList);
		ignoreItemListView.setAdapter(ignoreItemsAdapter);

		updateButton = (ImageButton)findViewById(R.id.updateButtonBtn);

		registerForContextMenu(ignoreItemListView);


		ignoreItemsAdapter.notifyDataSetChanged();

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		updateButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int itemCount = ignoreItemListView.getCount();			
				for(int i=0 ; i<itemCount; i++){
					Item itemAfterIgnore = (Item) ignoreItemListView.getItemAtPosition(i);

					View updatedItemView = ignoreItemListView.getChildAt(i);					
					EditText refillAfterEditText = (EditText) updatedItemView.findViewById(R.id.ignoreItemRefillAfter);
					
					itemAfterIgnore.setRefillAfter(Integer.parseInt(refillAfterEditText.getText().toString()));
					itemAfterIgnore.setNeedsRefill(false);
					try {
						dataProvider.saveItem(itemAfterIgnore);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				navigateBackToParent();
				ignoreItemsList.clear();				
			}

		});
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home: {navigateBackToParent();}		
		}		
		return super.onOptionsItemSelected(item);
	}

	private boolean navigateBackToParent(){
		Intent upIntent = NavUtils.getParentActivityIntent(this);
		if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
			TaskStackBuilder.create(this)
			.addNextIntentWithParentStack(upIntent)
			.startActivities();
		} else {
			NavUtils.navigateUpTo(this, upIntent);
		}
		return true;
	}

}
