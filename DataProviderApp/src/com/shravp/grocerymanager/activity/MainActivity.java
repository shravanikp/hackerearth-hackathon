package com.shravp.grocerymanager.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import com.parse.Parse;
import com.parse.ParseException;
import com.shravp.grocerymanager.R;
import com.shravp.grocerymanager.adapter.NavigationDrawerAdapter;
import com.shravp.grocerymanager.adapter.ShoppingListExpandableAdapter;
import com.shravp.grocerymanager.model.Item;
import com.shravp.grocerymanager.model.ItemType;
import com.shravp.grocerymanager.model.NavigationDrawerItem;
import com.shravp.grocerymanager.parse.util.ParseDataProvider;
import com.shravp.grocerymanager.service.SilentDataReceiver;
import com.shravp.grocerymanager.util.UserMessages;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.ExpandableListActivity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.telephony.SmsManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.Toast;


public class MainActivity extends ExpandableListActivity {
	private ShoppingListExpandableAdapter listAdapter;
	private ExpandableListView expListView;
	private List<ItemType> listDataHeader = new ArrayList<ItemType>();
	private HashMap<ItemType, List<Item>> listDataChild = new HashMap<ItemType,List<Item>>();
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mTitle;
	private NavigationDrawerAdapter adapter;
	private List<NavigationDrawerItem> dataList;
	private ParseDataProvider dataProvider;
	private ImageButton boughtButton;
	private ImageButton ignoreButton;
	private ImageButton smsButton;
	private Set<Item> checkedItems = new HashSet<Item>();
	public static final int PICK_CONTACT    = 1;
	public String SMS_SENT = "SMS_SENT";
	public String SMS_DELIVERED = "SMS_DELIVERED";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main); 
        
		
		Parse.initialize(this,getString(R.string.parse_application_id),getString(R.string.parse_client_id));

		dataProvider = new ParseDataProvider(this);

		expListView = (ExpandableListView) findViewById(android.R.id.list);		
		listAdapter = new ShoppingListExpandableAdapter(this, listDataHeader, listDataChild);        		
		expListView.setAdapter(listAdapter);
        
		boughtButton = (ImageButton)findViewById(R.id.boughtItemBtn);
		ignoreButton = (ImageButton)findViewById(R.id.ignoreItemBtn);
		smsButton = (ImageButton)findViewById(R.id.smsListBtn);

		expListView.setOnGroupClickListener(new OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {

				return false;
			}			
		});


		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
			@Override
			public void onGroupExpand(int groupPosition) {

			}
		});


		expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {
			@Override
			public void onGroupCollapse(int groupPosition) {

			}
		});


		expListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {				
				CheckedTextView checkbox = (CheckedTextView)v.findViewById(R.id.listItems);				
				Item selectedItem = listAdapter.getChild(groupPosition, childPosition);				
				checkbox.toggle();				
				if(checkbox.isChecked()){
					checkedItems.add(selectedItem);
				}else{
					checkedItems.remove(selectedItem);
				}

				return false;
			}
		});	

		try {
			refreshData();
		} catch (ParseException e) {		
			e.printStackTrace();
		}
	
		dataList = new ArrayList<NavigationDrawerItem>();	
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,GravityCompat.START);

		dataList.add(new NavigationDrawerItem(getString(R.string.title_drawer_1), R.drawable.shopping_cart));
		dataList.add(new NavigationDrawerItem(getString(R.string.title_drawer_2), R.drawable.item));
		adapter = new NavigationDrawerAdapter(this, R.layout.custom_drawer_item,dataList);

		mDrawerList.setAdapter(adapter);

		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.drawable.menu_icon, R.string.drawer_open,R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				invalidateOptionsMenu(); 				
			}

			public void onDrawerOpened(View drawerView) {
				invalidateOptionsMenu(); 
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);


		if (savedInstanceState == null) {
			selectItem(0);
		}


		boughtButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {				
				if(checkedItems.size() == 0){
					Toast.makeText(getApplicationContext(),UserMessages.selectAtleastOneItem.getDisplayMessage(),Toast.LENGTH_LONG).show();
					return;
				}
				else{									
					Intent boughtItemIntent = new Intent(MainActivity.this,BougthItemActivity.class);
					ArrayList<Item> boughtItemList = new ArrayList<Item>();
					for(Item bought : checkedItems){
						boughtItemList.add(bought);
					}
					boughtItemIntent.putParcelableArrayListExtra("boughtItems", boughtItemList);
					startActivity(boughtItemIntent);				
				}
			}
		});

		ignoreButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(checkedItems.size() == 0){
					Toast.makeText(getApplicationContext(),UserMessages.selectAtleastOneItem.getDisplayMessage(),Toast.LENGTH_LONG).show();
					return;
				}else{									
					Intent ignoreItemIntent = new Intent(MainActivity.this,IgnoreItemActivity.class);
					ArrayList<Item> ignoreItemList = new ArrayList<Item>();
					for(Item toIgnore : checkedItems){
						ignoreItemList.add(toIgnore);
					}
					ignoreItemIntent.putParcelableArrayListExtra("ignoreItems", ignoreItemList);
					startActivity(ignoreItemIntent);				
				}
			}
		});

		smsButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(intent, PICK_CONTACT);


			}		 
		});



		scheduleDataCleansing();
	}


	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);
		String phoneNumer = null;
		switch (reqCode) {
		case (PICK_CONTACT) :
			if (resultCode == Activity.RESULT_OK) {

				Uri contactData = data.getData();
				Cursor c =  managedQuery(contactData, null, null, null, null);
				if (c.moveToFirst()) {


					String id =c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

					String hasPhone =c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

					if (hasPhone.equalsIgnoreCase("1")) {
						Cursor phones = getContentResolver().query( 
								ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null, 
								ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id, 
								null, null);
						phones.moveToFirst();
						phoneNumer = phones.getString(phones.getColumnIndex("data1"));

					}
					String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));					
					sendSMSMessage(phoneNumer);
				}
			}
		break;
		}
	}


	private void sendSMSMessage(String phoneNumer) {		
		String message = "We need to buy these items";
		for(Entry<ItemType, List<Item>> entry : listDataChild.entrySet()) {
			for(Item required: entry.getValue()){
				message.concat(required.getName()).concat(",");
			}
		}

		
		SmsManager smsManager = SmsManager.getDefault();
		PendingIntent sentPendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(SMS_SENT), 0);
		PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(SMS_DELIVERED), 0);

		ArrayList<String> smsBodyParts = smsManager.divideMessage(message);
		ArrayList<PendingIntent> sentPendingIntents = new ArrayList<PendingIntent>();
		ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<PendingIntent>();

		for (int i = 0; i < smsBodyParts.size(); i++) {
			sentPendingIntents.add(sentPendingIntent);
			deliveredPendingIntents.add(deliveredPendingIntent);
		}

		// For when the SMS has been sent
		registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(context, "SMS sent successfully", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(context, "Generic failure cause", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(context, "Service is currently unavailable", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Toast.makeText(context, "No pdu provided", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(context, "Radio was explicitly turned off", Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(SMS_SENT));


		smsManager.sendMultipartTextMessage(phoneNumer, null, smsBodyParts, sentPendingIntents, deliveredPendingIntents);
	}

	public void selectItem(int position) {		
		switch (position) {
		case 0: break;
		case 1:{
			Intent categoryIntent = new Intent(this, ManageItemActivity.class);
			startActivity(categoryIntent);
			break;
		}

		}
		mDrawerList.setItemChecked(position, true);
		setTitle(dataList.get(position).getItemName());
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);		
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}


	private void refreshData() throws ParseException {	
		listDataChild.clear();
		listDataHeader.clear();
		setProgressBarIndeterminateVisibility(true);

		List<Item> itemsToShop = dataProvider.getItemsToShop();
		for(Item itemToShop: itemsToShop){
			setProgressBarIndeterminateVisibility(false);
			if(!listDataHeader.contains(itemToShop.getItemType())){
				listDataHeader.add(itemToShop.getItemType());
				List<Item> itemsList = new ArrayList<Item>();
				itemsList.add(itemToShop);
				listDataChild.put(itemToShop.getItemType(),itemsList);
			}
			else{				
				(listDataChild.get(itemToShop.getItemType())).add(itemToShop);				
			}
		}

		listAdapter.notifyDataSetChanged();
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}		
		return super.onOptionsItemSelected(item);
	}

	private class DrawerItemClickListener implements
	ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);

		}
	}


	public void scheduleDataCleansing() {
		Intent intent = new Intent(getApplicationContext(), SilentDataReceiver.class);		
		final PendingIntent pIntent = PendingIntent.getBroadcast(this, SilentDataReceiver.REQUEST_CODE,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);		
		long firstMillis = System.currentTimeMillis(); // first run of alarm is immediate
		int intervalMillis = 1000*60*60*24; // After 1 day
		AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, intervalMillis, pIntent);
		
		listAdapter.notifyDataSetChanged();
	}
}
